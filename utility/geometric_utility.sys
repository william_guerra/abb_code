MODULE geometric_utility (SYSMODULE)
    ! MIT License
    !
    ! Copyright (c) 2018 William Guerra <william.guerra@mr-robotica.it>
    !
    ! Permission is hereby granted, free of charge, to any person obtaining a copy
    ! of this software and associated documentation files (the "Software"), to deal
    ! in the Software without restriction, including without limitation the rights
    ! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    ! copies of the Software, and to permit persons to whom the Software is
    ! furnished to do so, subject to the following conditions:
    !
    ! The above copyright notice and this permission notice shall be included in all
    ! copies or substantial portions of the Software.
    !
    ! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    ! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    ! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    ! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    ! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    ! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    ! SOFTWARE.
    !
    !***************************************************************************
    ! Geometric related utility
    !
    ! Author: William Guerra <william.guerra@mr-robotica.it>
    !         MR Robotica
    !***************************************************************************
    !
    ! This function return TRUE if point is inside a box defined by cornerL and
    ! cornerH, FALSE if point is outside the box.
    ! The box is oriented as the base frame.
    FUNC bool insideBox(pos point, pos cornerL, pos cornerH)
        VAR num tmp;

        ! Order coordinates
        IF cornerL.x > cornerH.x THEN
            tmp := cornerL.x;
            cornerL.x := cornerH.x;
            cornerH.x := tmp;
        ENDIF
        IF cornerL.y > cornerH.y THEN
            tmp := cornerL.y;
            cornerL.y := cornerH.y;
            cornerH.y := tmp;
        ENDIF
        IF cornerL.z > cornerH.z THEN
            tmp := cornerL.z;
            cornerL.z := cornerH.z;
            cornerH.z := tmp;
        ENDIF

        ! Check if point is inside the box
        IF (point.x > cornerL.x AND point.x < cornerH.x) THEN
            IF (point.y > cornerL.y AND point.y < cornerH.y) THEN
                IF (point.z > cornerL.z AND point.z < cornerH.z) THEN
                    RETURN TRUE;
                ENDIF
            ENDIF
        ENDIF
        RETURN FALSE;
    ENDFUNC

    ! This function return TRUE if point is inside a sphere centered on center
    ! with a radius, FALSE if point is outside the sphere.
    FUNC bool insideSph(pos point, pos center, num radius)
        ! Evaluate if lenght of vector start from center of sphere and pointing
        ! to point is less than radius
        RETURN (VectMagn(center - point) <= radius);
    ENDFUNC

    ! This function return TRUE if point is inside a cylinder oriented as the
    ! vector joining startPt and endPt with a given base radius
    FUNC bool insideCyl(pos point, pos startPt, pos endPt, num radius)
        VAR pos v1;
        VAR pos v2;
        VAR num dsq;
        VAR num lenght;
        VAR num dot;

        ! Translate vectors origins to startPt
        v1 := endPt - startPt;
        v2 := point - startPt;

        lenght := Pow(VectMagn(v1), 2);
        dot := v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;

        IF dot < 0 OR dot > lenght THEN
            ! The v2 pojection parallel to v1 is outside the caps of the cylinder
            RETURN FALSE;

        ELSE
            ! v2 maybe is inside the cylinder, check if the v2 part orthogonal to v1 is less than radius
            dsq := Pow(VectMagn(v2), 2) - dot * dot / lenght;
            RETURN dsq < radius;
        ENDIF
    ENDFUNC

    ! This function use insideSph to determine if a point given as robtarget
    ! is near another point
    FUNC bool nearPoint(robtarget curPos, robtarget destPt, num drift)
        RETURN insideSph(curPos.trans, destPt.trans, drift);
    ENDFUNC

    ! This function determine if a given point is near a linear path
    FUNC bool nearLine(robtarget curPos, robtarget startPt, robtarget endPt, num drift)
        VAR bool result := FALSE;

        ! First check if point is near starting or final position (this require
        ! less computational power)
        result := nearPoint(curPos, startPt, drift) OR nearPoint(curPos, startPt, drift);
        IF result = FALSE THEN
            ! If not try to search between starting and ending position with a cylinder
            result := insideCyl(curPos.trans, startPt.trans, endPt.trans, drift);
        ENDIF
        RETURN result;
    ENDFUNC

    ! This function determine if a given point is inside a given linear path (an
    ! array of positions) and return the array position of the nearest point
    FUNC num nearPath(robtarget curPos, robtarget path{*}, num maxElements, num drift)
        VAR num i;
        VAR robtarget startPt;
        VAR robtarget endPt;

        ! retrive the first point from path
        endPt := popFifo(path, i \limit := maxElements);

        WHILE i < maxElements DO
            ! shift staring and ending positions
            startPt := endPt;
            endPt   := popFifo(path, i \limit := maxElements);

            ! Evaluate a branch of path
            IF nearLine(curPos, startPt, endPt, drift) THEN
                RETURN i - 1;
            ENDIF
        ENDWHILE

        ! If robot position is away from path return -1;
        RETURN -1;
    ENDFUNC

    ! This function return the normalized versor of a given vector
    FUNC pos normalize(pos vector)
        VAR num mag;

        mag := VectMagn(vector);
        vector.x := vector.x / mag;
        vector.y := vector.y / mag;
        vector.z := vector.z / mag;
        RETURN vector;
    ENDFUNC

    ! This function return the product between a vector and a scalar value
    FUNC pos magnVersor(pos versor, num magn)
        versor.x := versor.x * magn;
        versor.y := versor.y * magn;
        versor.z := versor.z * magn;
        RETURN versor;
    ENDFUNC

    ! This function traslate a point from a given tool and workobject to another
    ! Usage:
    ! For traslate only the workobject omit oldTool and newTool parameters
    ! For traslate only the tool omit oldWobj and newWobj parameters
    ! If an optional parameter is missing wobj0 or tool0 are used. Do not worry if you are translating a point betheen
    ! workobjects and you use tool0 instead of your tool: is safe to write:
    !     new_point = traslatePoint(old_point \oldWobj := old_w, \newWobj := new_w);
    ! even if old_point was touchup with a different tool than tool0
    FUNC robtarget traslatePoint(robtarget point, \PERS tooldata oldTool, \PERS wobjdata oldWobj \PERS tooldata newTool \PERS wobjdata newWobj)
        VAR pose p;
        VAR pose w;
        VAR pose t;

        p.trans := point.trans;
        p.rot   := point.rot;

        IF Present(oldTool) THEN
            t := oldTool.tframe;
        ELSE
            t := tool0.tframe;
        ENDIF

        IF Present(oldWobj) THEN
            w := oldWobj.uframe;
        ELSE
            w := wobj0.uframe;
        ENDIF

        ! Ok let's do some magic (quaternions are your friends...)
        p := PoseMult(PoseMult(w, p), PoseInv(t));

        IF Present(newTool) THEN
            t := newTool.tframe;
        ELSE
            t := tool0.tframe;
        ENDIF

        IF Present(newWobj) THEN
            w := newWobj.uframe;
        ELSE
            w := wobj0.uframe;
        ENDIF

        ! Just a bit more magic...
        p := PoseMult(PoseMult(PoseInv(w), p), t);

        point.trans := p.trans;
        point.rot   := p.rot;

        ! And voilà!
        RETURN point;
    ENDFUNC
ENDMODULE
