# Readme
***
In questo repository sono presenti alcuni esempi di codice scritti in RAPID per i controller robot ABB IRC5. Sono presenti anche dei moduli contenenti alcune procedure e funzioni che mi sono tornate utili nei programmi.

Se trovate errori segnalatemelo nell'apposita sezione di bug tracking, ogni contributo è ben accettato.

Tutto il software che trovate nel repository è liberamente utilizzabile e scaricabile e viene rilasciato sotto licenza MIT.
