MODULE MainModule
    ! ************************************************************************
    ! Programma di esempio di comunicazione tramite socket TCP/IP
    !
    ! Author: William Guerra <william.guerra@mr-robotica.it>
    ! Link  : https://www.bitbucket.org/william_guerra/abb_code
    ! ************************************************************************
    !
    ! In questo modulo di programma riporto un esempio di comunicazione tra controller robot e PLC e/o computer
    ! collegato in rete tramite socket TCP/UDP.
    !
    ! Il modo di comunicazione diventa utile se di devono condividere informazioni diverse da soli numeri e/o segnali
    ! con un dispositivo esterno come potrebbe esserlo un pc.
    !
    ! La gestione del socket è lasciata al programma pertanto consiglio di mettere tutto in un task parallelo in modo da
    ! snellire il programma principale. La comunicazione tra task può essere fatta tramite segnali virtuali e/o dati
    ! comuni ai task o ancora come di messaggi RMQ.

    LOCAL VAR socketdev server_socket;
    LOCAL VAR socketdev incoming_client;

    PROC main()
        !
        ! Creo un socket come server
        createSocket;

        ! Al client che si connette
        testSocket;
        destroySocket;
        RETURN;
    ENDPROC

    PROC createSocket()
        ! La procedura crea un socket e lo collega alla porta e indirizzo passati alla procedura SocketBind
        ! Notare che per la creazione di socket come server l'indirizzo IP dev'essere lo stesso del controller,
        ! non sono ammessi inidirizzi diversi
        SocketCreate server_socket;
        SocketBind server_socket, "127.0.0.1", 1025;

        ! Dopo aver creato il socket e averlo collegato alla porta lo metto in ascolto per le nuove connessioni
        SocketListen server_socket;

        ! Nuova connessione, accetto la connessione e passo un socket per la connessione
        SocketAccept server_socket, incoming_client;
        RETURN;
    ENDPROC

    PROC testSocket()
        VAR string message;

        WHILE TRUE DO
            ! Rimango in attesa di un messaggio da parte del client, quando arriva lo scrivo nella FlexPendant
            SocketReceive incoming_client \Str := message;
            TPWrite "Received: " + message;

            ! Controllo se corrisponde alla stringa "Hello Robot", se si rispondo e termino la connessione
            ! ATTENZIONE: usando la condizione di if seguente si evita di dover tenere conto del terminatore di stringa
            ! \00. Una comparazione usando il simbolo = necessita di tenere in considerazione il terminatore
            IF StrMatch(message, 1, "Hello Robot") = 1 THEN
                SocketSend incoming_client \Str := "Hello human";
                RETURN;
            ENDIF
        ENDWHILE
    ENDPROC

	PROC destroySocket()
        ! Chiudo il server e la connessione con il client in corso
		SocketClose server_socket;
		SocketClose incoming_client;
	ENDPROC
ENDMODULE
