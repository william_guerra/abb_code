MODULE advanced_proc
    ! ************************************************************************
    ! Programma esempio di passaggio dati avanzato
    !
    ! Author: William Guerra <william.guerra@mr-robotica.it>
    ! Link  : https://www.bitbucket.org/william_guerra/abb_code
    ! ************************************************************************
    !
    ! In questo modulo riporto alcuni esempi di passaggio avanzato di parametri alle procedure. Tali metodi non sono da
    ! considerarsi obbligatori, possono essere utili in alcune circostanze per ridurre la complessità del codice o la
    ! riusabilità di parti di esso.
    !
    ! Questo modulo vuole essere un esempio di applicazione di quella che viene chiamata visibilità dei dati nelle varie
    ! parti del codice
    PROC example()
        VAR num n1  := 21;
        VAR num n2  := 42;
        PERS num p1 := 21;
        PERS num p2 := 42;

        TPErase;
        TPWrite "Dati iniziali";
        TPWrite "Numero 1 " + NumToStr(p1, 0);
        TPWrite "Numero 2 " + NumToStr(p2, 0);

        swap_1 p1, p2;
        TPWrite "Dati dopo swap_1";
        TPWrite "Numero 1 " + NumToStr(p1, 0);
        TPWrite "Numero 2 " + NumToStr(p2, 0);

        swap_2 p1, p2;
        TPWrite "Dati dopo swap_2";
        TPWrite "Numero 1 " + NumToStr(p1, 0);
        TPWrite "Numero 2 " + NumToStr(p2, 0);

        swap_3 p1, p2;
        TPWrite "Dati dopo swap_3";
        TPWrite "Numero 1 " + NumToStr(p1, 0);
        TPWrite "Numero 2 " + NumToStr(p2, 0);

        ! Se si decommenta la riga di codice successiva e si cerca di salvare la modifica si ottiene un errore da parte
        ! di rapid a causa del passaggio di un dato di tipo var anzichè pers
        !swap_3 n1, n2;
        RETURN;
    ENDPROC

    PROC swap_1(num numero1, num numero2)
        ! La procedura esegue, o tenta di eseguire, lo swap dei due dati passati alla procedura al momento della sua
        ! chiamata.
        !
        ! i dati numero1 e numero2 sono delle copie dei dati passati al momento della chiamata della procedura, la
        ! modifica dei dati all'interno della procedura, pertanto, avviene su questa copia senza essere propagata ai
        ! dati della procedura chiamante.
        !
        ! Il risultato finale è quindi una funzione inutile
        VAR num tmp;

        tmp := numero1;
        numero1 := numero2;
        numero2 := tmp;

        ! N.B. A partire dai sistemi S4 (credo) non è più obbligatorio l'uso del return al termine delle procedure
        ! Io, personalmente lo uso per comodità in fase di debug con FlexPendant in quanto mi mette a disposizione
        ! una riga di codice selezionabile in cui spostare il puntatore qualora volessi saltare la procedura
        RETURN;
    ENDPROC

    PROC swap_2(INOUT num numero1, INOUT num numero2)
        ! La procedura esegue lo swap dei due numeri passati alla procedura al momento della sua chiamata.
        !
        ! Questa procedura, a differenza della swap_1, funziona in quanto l'attributo INOUT comporta il passaggio non di
        ! una copia del dato, bensì viene passato un riferimento al dato stesso. In altre parole è un modo per
        ! permettere alla procedura di accedere direttamente al dato della procedura chiamante
        VAR num tmp;

        tmp := numero1;
        numero1 := numero2;
        numero2 := tmp;

        RETURN;
    ENDPROC

    PROC swap_3(PERS num numero1, PERS num numero2)
        ! La procedura si comporta allo stesso modo della procedura swap_2 solo che richiede obbligatoriamente il
        ! passaggio di due dati di tipo PERS, mentre la procedura precedente ammette il passaggio di due dati di tipo
        ! VAR
        VAR num tmp;

        tmp := numero1;
        numero1 := numero2;
        numero2 := tmp;

        RETURN;
    ENDPROC
ENDMODULE
