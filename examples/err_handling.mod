MODULE err_handling
    ! ************************************************************************
    ! Programma di esempio per la gestione errori tramite error handler
    !
    ! Author: William Guerra <william.guerra@mr-robotica.it>
    ! Link  : https://www.bitbucket.org/william_guerra/abb_code
    ! ************************************************************************
    !
    ! In questo modulo di programma riporto un esempio di gestione errori tramite error handling. Questo metodo che
    ! considero molto elegante permette di uscire da situazioni intricate facilmente, in alcuni casi invece risulta
    ! indispensabile per risolvere alcuni problemi.
    !
    ! Prima un po' di info che spero possano essere utili: l'error handling permette di scrivere codice che si occuperà
    ! di gestire situazioni considerate errori del programma, non intesi come errori sintattici (sbaglio a scrivere
    ! un'istruzione) bensì errori che si generano durante l'esecuzione del programma. Es.: Divisione tra due numeri che
    ! l'operatore mi passa tramite FlexPendant, l'operatore sbaglia (o lo fa di proposito) e iserisce come divisore 0.
    ! L'operazione non ammette soluzioni (in R) e il programma si blocca a casusa dell'errore. Con l'error handling voi
    ! potete ``catturare'' questa condizione e gestirla (es.: scrivete a monitor che i dati sono sbagliati).
    !
    ! Non limitatevi a immaginare l'errore come qualcosa di non voluto, ma pesatelo come evento che può verificarsi
    ! durante la normale esecuzione del programma. Per esempio potete giocare con il gestore di collisioni in modo da
    ! sapere quando avete toccato qualcosa (tip: potete ``cercare'' un pezzo o un piano in questo modo).
    !
    ! Ok, fatta la premessa passiamo al programma: esiste una lista di errori che si possono generare durante
    ! l'esecuzione di un programma, questa lista si trova a pagina 1599 del ``Manuale tecnico di riferimento -
    ! Istruzioni, funzioni e tipi di dati in RAPID'', inoltre ogni istruzione, sempre nello stesso manuale, riporta gli
    ! errori che può generare.
    !
    ! Come se non bastasse potete generare anche voi errori (no, non scrivendo cose a caso).
    !
    ! Iniziamo!
    !
    ! Questa è una variablie di errore, dev'essere definita da qualche parte nel programma con valore iniziale -1 e
    ! tassativamente come variabile. Questa variabile serve solo se volete creare i vostri errori
    VAR errnum errore_1 := -1;
    VAR errnum errore_2 := -1;

    PROC err_init()
        ! L'istruzione riserva un numero di errore alla variabile che avete creato. Sostanzialmente assegna un numero
        ! univoco alla variabile in modo che sia riconoscibile dal sistema.
        !
        ! Essendo il dato di tipo variabile dovrete lanciare la routine ogni volta che fate partire o ripartire il
        ! programma, quindi dovrete creare un ``event routine'' (configurazione -> Controller -> Event Routine) che
        ! lanci la seguente procedura quando premete Start, Restart o Step.
        !
        ! Vi consiglio quindi di creare una procedura separata come in questo esempio da collegare all'evento
        BookErr errore_1;
        BookErr errore_2;
    ENDPROC

    PROC generate_error()
        ! Con l'istruzione RAISE alzate l'errore, ovvero chiamate il gestore degli errori
        RAISE errore_1;
        TPWrite "Welcome back!";
        RAISE errore_2;
        RETURN;
    ERROR
        ! Dopo l'esecuzione di qualsiasi istruzione RAISE all'interno della routine il puntatore verrà spostato qui
        ! dentro
        IF ERRNO = errore_1 THEN
            TPErase;
            TPWrite "Houston we have had an error!";
            ! Scusate non ho potuto non fare questa battuta
            !
            ! L'istruzione TRYNEXT dice a RAPID di ripartire con l'istruzione successiva a quella che ha generato
            ! l'errore, in questo caso il TPWrite dopo RAISE
            TRYNEXT;
        ELSEIF ERRNO = errore_2 THEN
            TPWrite "This is a matrix glitch!";
            ! Altra semi citazione
            !
            ! In questo caso l'istruzione RETRY dice a RAPID di riprendere l'esecuzione del programma dalla stessa
            ! istruzione che ha generato l'errore.
            RETRY;
        ELSE
            ! Uh, questo errore è inatteso (nessuna delle condizioni precedenti lo ha preso in carico). Il comando RAISE
            ! alza l'errore al gestore errori della routine chiamante
            RAISE;
        ENDIF
    ENDPROC

    PROC my_proc_1()
        ! Inizializzazione delle variabili di errore
        err_init;

        TPWrite "Esecuzione del programma";
        TPWrite "Ora chiamo la procedura per ";
        TPWrite "creare deliberatamente un errore";

        generate_error;
        RETURN;
    ENDPROC
ENDMODULE
