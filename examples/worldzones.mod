MODULE worldzones
    ! ************************************************************************
    ! Programma di esempio di gestione di worldzones
    !
    ! Author: William Guerra <william.guerra@mr-robotica.it>
    ! Link  : https://www.bitbucket.org/william_guerra/abb_code
    ! ************************************************************************
    !
    ! In questo modulo riporto alcuni esempi di come creare delle worldzones e di come usarle all'interno del
    ! programma.
    !
    ! Cerchero' di mostrare la differenza tra i vari modi di creare le worldzones e cosa comportano.
    !
    ! Le worldzones permettono di creare volumi a forma sferica, cilindrica, parallelepipedi o posizioni assolute dei
    ! giunti del robot le quali possono essere usate per impedire l'ingresso o l'uscita del robot da delle aree di
    ! lavoro oppure l'impostazione di segnali per la segnalazione di fuori ingombro.
    ! 
    ! Le worldzones possono essere create con due modalita' differenti: 
    !   - Worldzones fisse o stazionarie (stationary worldzones)
    !   - Worldzones temporanee
    !
    ! Entrambe devono essere create dal programma, ma solo quelle temporanee possono essere disattivate temporaneamente
    ! o cancellate definitivamente e riassegnate. Quelle statiche rimangono definite fino al riavvio del controller
     
    ! Spigoli opposti del paralellepipedo
    CONST pos boxL  := [100, 100, 100];
    CONST pos boxH  := [200, 200, 200];
    
    ! Base, raggio e altezza del cilindro
    CONST pos cylB  := [100, 100, 100];
    CONST num cylR  := 100;
    CONST num cylH  := 200;
    
    ! Centro e raggio della sfera
    CONST pos sphC  := [100, 100, 100];
    CONST num sphR  := 200;
    
    ! Posizione assoluta degli assi e la tolleranza di posizionamento
    CONST jointtarget jpos  := [[0, 0, 0, 0, 0, 0],[0, 9e9, 9e9, 9e9, 9e9, 9e9]];
    CONST jointtarget delta := [[2, 2, 2, 2, 2, 2],[2, 9e9, 9e9, 9e9, 9e9, 9e9]];

    VAR wzstationary box1;
    VAR wzstationary cyl1;
    VAR wztemporary sph1;
    VAR wztemporary jnt1;
    
    ! Segnali da collegare tramite alias a segnali configurati nel controller.
    ! I segnali usati con l'istruzione WZDOSet devono essere esclusivamente "ReadOnly", non sono ammessi segnali con
    ! accesso diverso, pena errore bloccante in fase d'esecuzione dell'istruzione.
    VAR signaldo DO_ingombro;
    VAR signaldo DO_home;

    PROC createStatWzones()
        ! Variabile temporanea di appoggio per la creazione delle worldzones.
        ! Mantiene, come si puo' intuire dal nome, le informazioni del volume che si vuole collegare ad una worldzone.
        !
        ! N.B. Considerazione sulla pulizia del codice.
        ! shapedata non è necessario definirla globale, in quanto se si segue il metodo di creazione che riporto nella
        ! procedura (il quale è il metodo, da quanto ho visto, preferito) la variabile serve per due istruzioni,
        ! dopodiche' diventa superflua e riutilizzabile.
        VAR shapedata volume;
        !
        ! Definizione di un parallelepipedo con spigoli opposti nei punti boxL e boxH, il volume e' definito all'interno
        ! del parallelepipedo. Notare che la prima istruzione non definisce la worldzone, ma crea solo il volume che
        ! sara' poi usato per la worldzone vera e propria.
        WZBoxDef \inside, volume, boxL, boxH;
        !
        ! Creazione della worldzone vera e propria: in questo caso, con il volume creato precedentemente viene creato un
        ! volume nel quale al robot non è concesso entrare.
        WZLimSup \Stat, box1, volume;
        !
        WZCylDef \Inside, volume, cylB, cylR, cylH;
        ! Creazione di una worldzone per l'impostazione di un segnale quando il robot entra o esce dalla zona
        ! (cilindrica in questo esempio). Notare che quando il robot entra nel cilindro il segnale viene impostato a 0
        ! mentre quando il robot esce il segnale torna a 1. 
        !
        ! Questo e' un esempio di gestione di fuori ingombro, usando la logica inversa in modo da evitare di essere
        ! sicuri che il robot si trovi effettivamente fuori dall'ingombro della macchina anche a controller spento o in
        ! errore di sistema.
        WZDOSet \Stat, cyl1, \Inside, volume, DO_ingombro, low;
        !
        !
        ! Le Worldzone create con questa procedura non sono piu' modificabili o eliminabili fino al riavvio del
        ! controller.
        ! 
        ! Lanciando due volte la procedura si ottiene un errore in quanto il controller ha gia' creato le wordzone,
        ! quindi inserire la procedura nel main non e' una bella idea! L'approccio corretto e' quello di configurare la
        ! procedura in modo da essere lanciata al solo avvio del controller (configurazione > Controller > Routine
        ! d'evento)
    ENDPROC
    
    PROC createTmpWzones()
        VAR shapedata volume;
        !
        ! Eliminazione delle worldzone precedentemente create. Le worldzones verranno completamente rimosse dal
        ! controller, i relativi nomi (sph1 e jnt1) sono quindi nuovamente collegabili ad altre worldzones.
        ! 
        ! Notare che nel caso le worldzones non siano create la procedura non ritorna errori, invece tentare di creare
        ! una worldzone con un nome già assegnato genera un errore bloccante.
        WZFree sph1;
        WZFree jnt1;
        !
        ! In questa procedura vengono create delle worldzones temporanee le quali possono essere messe in sleep o
        ! cancellate o modificate senza dover riavviare il controller. Inoltre, allo spostamento del puntatore di
        ! programma in una routine diversa (tramite "PP in main" o "PP in routine")
        WZSphDef \Inside, volume, sphC, sphR;
        WZLimSup \Temp, sph1, volume;
        !
        WZHomeJointDef \Inside, volume, jpos, delta;
        WZDOSet \Temp, jnt1, \Inside, volume, DO_home, high;

        ! Abilitazione di una worldzone temporanea. Dopo il comando la worldzone viene supervisionata dal controller.
        WZEnable jnt1;
        ! Disabilitazione di una worldzone temporanea. Dopo il comando la worldzone non viene piu' supervisionata dal
        ! controller, non viene pero' eliminata; pertanto e' possibile riattivarla con "WZEnable" senza doverla ricreare.
        WZDisable jnt1;
    ENDPROC
ENDMODULE
